:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: list-utils.scm
Provides some utility procedures for scheme lists.

:arrow_right: [Go to source code of this file.](/src/list-utils.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: append-to-list](#append-to-list) | Appends a value to the given list. |
| [:page_facing_up: map-get-value](#map-get-value) | Retrieves the value of a key in a map. A map is a list of pairs (lists witht... |
| [:page_facing_up: map-set-value](#map-set-value) | Sets the value of a key in a map. A map is a list of pairs (lists with twoen... |

## Procedure Documentation

### append-to-list

#### Syntax
```scheme
(append-to-list lst value)
```

:arrow_right: [Go to source code of this procedure.](/src/list-utils.scm#L12)

#### Export Name
`<import-name>/append-to-list`

#### Description
Appends a value to the given list.

#### Parameters
##### `lst`  
The list to which the value should be appended.

##### `value`  
The value to append.


#### Returns
True if successfully appended the value, false otherwise.

-------------------------------------------------
### map-get-value

#### Syntax
```scheme
(map-get-value map key)
```

:arrow_right: [Go to source code of this procedure.](/src/list-utils.scm#L40)

#### Export Name
`<import-name>/map-get-value`

#### Description
Retrieves the value of a key in a map. A map is a list of pairs (lists with
two entries) and a pair consists of a key (first entry) and a corresponding
value (second entry).

#### Parameters
##### `map`  
The map from which to retrieve the value of the key.

##### `key`  
The key from which to read the value.


#### Returns
The value of the key or if the key was not found than the symbol
`'key-not-found`.

-------------------------------------------------
### map-set-value

#### Syntax
```scheme
(map-set-value map key value)
```

:arrow_right: [Go to source code of this procedure.](/src/list-utils.scm#L67)

#### Export Name
`<import-name>/map-set-value`

#### Description
Sets the value of a key in a map. A map is a list of pairs (lists with two
entries) and a pair consists of a key (first entry) and a corresponding value
(second entry).

#### Parameters
##### `map`  
The map where to set the value of the key.

##### `key`  
The key in the map, where to write the value.

##### `value`  
The value for the key.



