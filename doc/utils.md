:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: utils.scm
Implements some general utility procedures.

:arrow_right: [Go to source code of this file.](/src/utils.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: transcipt-commands](#transcipt-commands) | Transcripts the given commands by using the fluent tui commands"file/start-t... |
| [:page_facing_up: file-content-to-list](#file-content-to-list) | This procedure reads the contents of a file and converts it to a list ofsche... |
| [:page_facing_up: get-fluent-version](#get-fluent-version) | Retrieve the current running fluent version. |
| [:page_facing_up: get-fluent-version-string](#get-fluent-version-string) | Retrieves the current running fluent version string. |
| [:page_facing_up: get-unique-file-path](#get-unique-file-path) | Generates a file name that does not exist in the given directory, based on t... |
| [:page_facing_up: cmd-output-to-object-list](#cmd-output-to-object-list) | Executes the given commands and returns the output of this commands as anobj... |
| [:page_facing_up: exec-on-bash](#exec-on-bash) | Executes the given commands on the bash. |
| [:page_facing_up: sys-cmd-output-to-object-list](#sys-cmd-output-to-object-list) | Executes the given system command and returns the output of this systemcomma... |
| [:page_facing_up: build-index-operation](#build-index-operation) | Builds an index operation depending on the given `indexDefinition`. The`inde... |
| [:page_facing_up: find-in-object-list](#find-in-object-list) | Searches for a value in the object list identified by a token and name. Thep... |
| [:page_facing_up: set-or-create-rpvar](#set-or-create-rpvar) | Sets the value of an rp variable. If the rp variable does not exist, than it... |
| [:page_facing_up: get-fluent-install-dir](#get-fluent-install-dir) | Get the fluent install directory, e.g. `"/software/ansys_inc/v162/fluent"`. |
| [:page_facing_up: get-fluent-solver-version](#get-fluent-solver-version) | Retrieves the current solver version, which is one of: `"2d"`, `"2ddp"`, `"... |

## Procedure Documentation

### transcipt-commands

#### Syntax
```scheme
(transcipt-commands commands outputfile)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L12)

#### Export Name
`<import-name>/transcipt-commands`

#### Description
Transcripts the given commands by using the fluent tui commands
"file/start-transcript" and "file/stop-transcript". The transcript will write
to the output file passed as parameter.

#### Parameters
##### `commands`  
The commands to execute, must be a function with no parameters.

##### `outputfile`  
The output file where to write the transcipt.



-------------------------------------------------
### file-content-to-list

#### Syntax
```scheme
(file-content-to-list inputfile)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L45)

#### Export Name
`<import-name>/file-content-to-list`

#### Description
This procedure reads the contents of a file and converts it to a list of
scheme objects. In the file the objects are seperated by whitespace.

#### Parameters
##### `inputfile`  
The file to read from.


#### Returns
The object list or false if the file does not exist.

-------------------------------------------------
### get-fluent-version

#### Syntax
```scheme
(get-fluent-version)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L403)

#### Export Name
`<import-name>/get-fluent-version`

#### Description
Retrieve the current running fluent version.

#### Returns
A key value list (map) with the following keys:  

   + `'version-string` - The fluent version string (`"<major>.<minor>.<patch>"`)
   + `'major` - The fluent major version.
   + `'minor` - The fluent minor version.
   + `'patch` - The fluent patch version.

-------------------------------------------------
### get-fluent-version-string

#### Syntax
```scheme
(get-fluent-version-string)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L425)

#### Export Name
`<import-name>/get-fluent-version-string`

#### Description
Retrieves the current running fluent version string.

#### Returns
The version string.

-------------------------------------------------
### get-unique-file-path

#### Syntax
```scheme
(get-unique-file-path directory basename [suffix])
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L447)

#### Export Name
`<import-name>/get-unique-file-path`

#### Description
Generates a file name that does not exist in the given directory, based on the
given parameters according to the following syntax:
`<directory>/<basename><index>[.<suffix>]`  
The unique file path is than returned.

#### Parameters
##### `directory`  
The directory for the file.

##### `basename`  
The prefix of the file name.

##### `suffix`  
_Attributes: optional, default: `""`_  
The file name extension.


#### Returns
The unique file path.

-------------------------------------------------
### cmd-output-to-object-list

#### Syntax
```scheme
(cmd-output-to-object-list commands)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L119)

#### Export Name
`<import-name>/cmd-output-to-object-list`

#### Description
Executes the given commands and returns the output of this commands as an
object list. See [transcipt-commands](#transcipt-commands) and 
[file-content-to-list](#file-content-to-list) for details.

#### Parameters
##### `commands`  
The commands to execute, must be a function with no parameters.


#### Returns
The output of the commands as an object list or false if the
transcript file does not exist.

-------------------------------------------------
### exec-on-bash

#### Syntax
```scheme
(exec-on-bash cmd)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L145)

#### Export Name
`<import-name>/exec-on-bash`

#### Description
Executes the given commands on the bash.

#### Parameters
##### `cmd`  
The commands to execute. If it is a file path to an existing file,
than the commands in that file will be executed. If not, than the
parameter is expected to be an string of commands to execute.


#### Returns
Zero on success, not zero otherwise.

-------------------------------------------------
### sys-cmd-output-to-object-list

#### Syntax
```scheme
(sys-cmd-output-to-object-list system-command [execdir])
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L178)

#### Export Name
`<import-name>/sys-cmd-output-to-object-list`

#### Description
Executes the given system command and returns the output of this system
command as an object list. System commands are commands that will be executed
on the current shell where fluent was started, see the fluent documentation
of the scheme command `system`.

#### Parameters
##### `system-command`  
A string with the system command, that will be executed on
the current shell.

##### `execdir`  
_Attributes: optional, default: `#f`_  
A path to the directory where the command
should be executed. If omitted, than the current directory will be used.


#### Returns
The output of the system command as an object list or false in case
of an error.

-------------------------------------------------
### build-index-operation

#### Syntax
```scheme
(build-index-operation indexDefinition)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L248)

#### Export Name
`<import-name>/build-index-operation`

#### Description
Builds an index operation depending on the given `indexDefinition`. The
`indexDefinition` has the following syntax: `(-|+)deltaIndex`  
e.g.: `"-1",` `"+3"`, `"-14"`, ...  
The procedure will generate a function that adds or subtracts the deltaIndex
from the passed number. e.g.: `"+3"` will generate a function that adds three
to the passed number and returns the result.

#### Parameters
##### `indexDefinition`  
The index definition as described above.


#### Returns
The generated operation (function).

-------------------------------------------------
### find-in-object-list

#### Syntax
```scheme
(find-in-object-list objectList name nameIdx token valueIdx numMatch)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L295)

#### Export Name
`<import-name>/find-in-object-list`

#### Description
Searches for a value in the object list identified by a token and name. The
procedure searches for an object that is equal to the token. If it has found
the token, than it checks nameIdx objects backwards if this object is equal
to name, if so it returns the object valueIdx forward to the token, e.g.:  

  + object-list = `(a b c d e f g e h i j k)`
  + token = `"f"`
  + nameIdx = `"-2"`
  + name = `"d"`
  + valueIdx = `"+3"`
  + => returned value = `"h"`

#### Parameters
##### `objectList`  
The object list to search in.

##### `name`  
The name to match against the name object.

##### `nameIdx`  
The index definition (see the 
[build-index-operation](#build-index-operation) procedure)
for the position of the name object relative from the position of the
token object.

##### `token`  
The token to match against the token value.

##### `valueIdx`  
The index definition (see the 
[build-index-operation](#build-index-operation) procedure)
for the position of the value object relative from the position of the
token object.

##### `numMatch`  
An integer number that defines which match should be returned.
e.g.: The fisrt match, or second or third and so on.


#### Returns
The value of the found object or the symbol `'not-found` if it was 
not found.

-------------------------------------------------
### set-or-create-rpvar

#### Syntax
```scheme
(set-or-create-rpvar name value type)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L357)

#### Export Name
`<import-name>/set-or-create-rpvar`

#### Description
Sets the value of an rp variable. If the rp variable does not exist, than it
will be created and initialized with the value.

#### Parameters
##### `name`  
The name of the rp variable.

##### `value`  
The value for the rp variable.

##### `type`  
The type of the rp variable, see the fluent documentation for
possible types.



-------------------------------------------------
### get-fluent-install-dir

#### Syntax
```scheme
(get-fluent-install-dir)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L369)

#### Export Name
`<import-name>/get-fluent-install-dir`

#### Description
Get the fluent install directory, e.g. `"/software/ansys_inc/v162/fluent"`.

#### Returns
Path to the fluent install directory as string.

-------------------------------------------------
### get-fluent-solver-version

#### Syntax
```scheme
(get-fluent-solver-version)
```

:arrow_right: [Go to source code of this procedure.](/src/utils.scm#L387)

#### Export Name
`<import-name>/get-fluent-solver-version`

#### Description
Retrieves the current solver version, which is one of:
  `"2d"`, `"2ddp"`, `"3d"`, `"3ddp"`

#### Returns
The solver version as string.

