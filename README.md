Utilities Module
================

The `fscm-utils` module provides some utility procedures that solve general
problems and therefore simplifying fluent scheme script development. Below the
installation and basic usage of this module is described, if you are looking for
the API documentation [click here](doc/README.md) (located in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Installation
------------
Before you can install the utils module you have to properly setup the module
manager. Follow the 
[Fluent Scheme Module Manager](https://gitlab.com/jkuisw/fscm-module-manager)
guide to set it up.

Now you need to clone the module repository to your computer. Choose a proper
location where you want to clone it, preferably the same directory where the
module manager repository is located (e.g.: `/home/<user name>/jkuisw`). Go into
that directory and clone the repository, as follows:

```shell
cd "/home/<user name>/jkuisw"
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-utils.git
```

After cloning the repository ensure that the `fscm-utils` module is registered
to the module manager, as described in the 
[module manager setup](https://gitlab.com/jkuisw/fscm-module-manager#register-modules) 
(`add-module-paths` procedure).

Usage
-----

To use the utils module you have to import it with a proper import name. After
that you can use the exported procedures of the module:

```scheme
; Import the utils module.
(import "utils" "fscm-utils")

; Using the utils module for example to split a string by a character.
(define str "abc-def-geh")
(utils/string-split str #\-)
```

Look into the [API documentation](doc/README.md) to see what features are 
provided by the utils module.
