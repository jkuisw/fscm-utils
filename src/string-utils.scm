;*******************************************************************************
; This part of the module implements utility procedures for string operations.

;*******************************************************************************
; Converts the given object to a string.
;
; Parameters:
;   obj - The object to convert.
;
; Returns: The object converted to a string.
(define (to-string obj)
  (cond
    ((string?  obj) obj)
    ((symbol?  obj) (symbol->string obj))
    ((boolean? obj) (format "~a" obj))
    ((integer? obj) (number->string obj))
    ((number?  obj) (number->string obj))
    ((real?    obj) (number->string obj))
    ((char?    obj) (string obj))
    (else "")
  )
)

;*******************************************************************************
; Converts an object to a symbol.
;
; Parameters:
;   obj - The object to convert.
;
; Returns: The object converted to a symbol.
(define (to-symbol obj)
  (cond
    ((symbol?  obj) obj)
    (else (string->symbol (to-string obj)))
  )
)

;*******************************************************************************
; Returns the substring of the given string by evaluating the given regular
; expression. For the regular expression matching the system command `grep` is
; used, therefore the supported regex features are the one of the "grep" system
; command.
;
; Parameters:
;   str - The string of which to extract the substring.
;   regex - The regular expression to match the substring in the string.
;
; Returns: The matched substring.
(define (get-substring-by-regex str regex) (let (
    (sys-cmd "")
    (result '())
  )
  (set! sys-cmd (format #f "echo \"~a\" | grep -P '~a' -o" str regex))

  (set! result (sys-cmd-output-to-object-list sys-cmd))
  (if (and (not (eqv? result #f)) (> (length result) 0))
    (list-ref result 0) ; Found substring.
    "" ; Failed or not found.
  )
))

;*******************************************************************************
; Replaces a substring in a given string by a given replacement string. The
; substring found by the given regular expression. This operation is done by the
; system command `sed`. Therefore the supported regex features are the one of
; the `sed` system command.
;
; Parameters:
;   str - The string where the substring should be replaced.
;   regex - The regular expression to find the substring.
;   replace - The replacement string.
;
; Returns: The string with the replaced substring or an empty string if the
;   substring could not be found.
(define (replace-substring-by-regex str regex replace) (let (
    (sys-cmd "")
    (result '())
  )
  (set! sys-cmd (format #f "echo \"~a\" | sed 's/~a/~a/g'" str regex replace))

  (set! result (sys-cmd-output-to-object-list sys-cmd))
  (if (and (not (eqv? result #f)) (> (length result) 0))
    (list-ref result 0) ; Found substring.
    "" ; Failed or not found.
  )
))

;*******************************************************************************
; Splits the given string by the given character.
;
; Parameters:
;   str - The string to split.
;   ch - The split character.
;
; Returns: A list of the substrings splitted by the given character.
(define (string-split str ch) (let ((len 0))
  ; Check parameters.
  (if (not (or (string? str) (symbol? str))) (error "Invalid parameter!" str))
  (if (not (char? ch)) (error "Invalid parameter!" ch))

  ; Convert str to string if symbol.
  (if (symbol? str) (set! str (to-string str)))
  (set! len (string-length str))

  ; Loop through the string character to split it by the character.
  (letrec (
      ; split - procedure to be called recursively with parameters:
      ;   a - position of the first or last character of a substring in the
      ;     string
      ;   b - character pointer in the string
      (split (lambda (a b)
        (cond
          ; Check if reached end of string.
          ((>= b len)
            (if (= a b)
              '()
              (cons (substring str a b) '())
            )
          )
          ; Check if current character (where b points to) is a split character.
          ((char=? ch (string-ref str b))
            (if (= a b)
              (split (+ 1 a) (+ 1 b))
              (cons (substring str a b) (split b b))
            )
          )
          ; Go to the next character in the string.
          (else (split a (+ 1 b)))
        )
      ))
    )
    (split 0 0)
  )
))

;*******************************************************************************
; Joins a list of string together to one string. When a seperator string is
; passed, than the strings will be joined with the seperator string between
; them.
;
; Parameters:
;   str-list - A list of strings.
;   seperator - [optional, default: ""] The seperator string that will be
;     inserted between the strings of the list.
;
; Returns: The joined string or false when failed.
(define (string-join str-list . args) (let (
    (seperator "")
    (tmp-str "")
    (str "")
  )
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Seperator string parameter passed.
    (set! seperator (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (or (string? seperator) (symbol? seperator)))
      (error "Invalid parameter!" seperator)
    )
  ))

  (let next-str ((remaining-strings str-list))
    (cond
      ; When remaining strings list is empty, return an empty string.
      ((null? remaining-strings) "")
      ; Check if current element is a string or symbol.
      ((not (or
          (string? (car remaining-strings))
          (symbol? (car remaining-strings))
        ))
        (error "Current list element is not a string or symbol!"
          (car remaining-strings))
      )
      ; If remaining string list only contains one element, do not append the
      ; seperator.
      ((= (length remaining-strings) 1) (begin
        (set! tmp-str (car remaining-strings))
        (if (symbol? tmp-str) (set! tmp-str (to-string tmp-str)))
        (set! str (string-append str tmp-str))
      ))
      ; Else append the current string with the seperator and go to the next
      ; one.
      (else (begin
        (set! tmp-str (car remaining-strings))
        (if (symbol? tmp-str) (set! tmp-str (to-string tmp-str)))
        (set! str (string-append str tmp-str seperator))
        (next-str (cdr remaining-strings))
      ))
    )
  )

  str ; Return the joined string.
))

;*******************************************************************************
; Converts all elements of the given list to strings.
;
; Parameters:
;   lst - The list to convert.
;
; Returns: A list with all elements converted to strings.
(define (object-list->string-list lst) (let (
    (str-list '())
  )
  (let next-elem ((remaining-elems lst))
    (cond
      ; If reached end of list, return the converted list.
      ((null? remaining-elems) str-list)
      ; Convert the next element.
      (else (begin
        (if (null? str-list)
          (set! str-list (list (to-string (car remaining-elems))))
          (append-to-list str-list (to-string (car remaining-elems)))
        )
        (next-elem (cdr remaining-elems))
      ))
    )
  )
))

;*******************************************************************************
; Removes all whitespace characters from the given string.
;
; Parameters:
;   str - The string from which to remove the whitespace characters.
;
; Returns: The string without whitespace characters
(define (string-remove-whitespace str) (let (
    (new-str-list '())
    (curr-char #\space)
    (len 0)
  )
  ; Chack if parameter is valid.
  (if (not (or (string? str) (symbol? str))) (error "Invalid parameter!" str))

  ; Initilaize variables.
  (if (symbol? str) (set! str (to-string str)))
  (set! len (string-length str))

  (if (> len 0)
    (set! new-str-list (let next-char ((char-pos 0))
      ; Check if reched end of string.
      (if (>= char-pos len) (begin
        '() ; Raeched end of string.
      ) (begin ; else
        (set! curr-char (string-ref str char-pos))

        ; Check if current character is a whitespace character.
        (if (char-whitespace? curr-char) (begin
          ; Current character is a whitespace character, therefore strip it.
          (next-char (+ char-pos 1))
        ) (begin ; else
          ; Current characteris not a whitespace character.
          (cons curr-char (next-char (+ char-pos 1)))
        ))
      ))
    ))
  )

  ; Return the new string without whitespace characters.
  (list->string new-str-list)
))

(define (replace-substring-by-regex str regex replace) (let (
    (sys-cmd "")
    (result '())
  )
  (set! sys-cmd (format #f "echo \"~a\" | sed 's/~a/~a/g'" str regex replace))

  (set! result (sys-cmd-output-to-object-list sys-cmd))
  (if (and (not (eqv? result #f)) (> (length result) 0))
    (list-ref result 0) ; Found substring.
    "" ; Failed or not found.
  )
))

;*******************************************************************************
; Checks if the given string matches the given glob-pattern.
;
; Parameters:
;   string - The string to match against.
;   glob-pattern - The globbing pattern the string should match against.
;
; Returns: True if the glob-pattern matches the string, false otherwise.
(define (does-string-match-glob? string glob-pattern) (let (
    (sys-cmd "")
    (result '())
  )
  (set! sys-cmd (string-append
    "if [[ \"" string "\" = " glob-pattern " ]]; then "
    "echo \"true\"; else echo \"false\"; fi"
  ))

  (set! result (sys-cmd-output-to-object-list sys-cmd))
  (if (and (not (eqv? result #f)) (> (length result) 0))
    (string=? (list-ref result 0) "true") ; Globbing has matched if true.
    #f ; An error occured.
  )
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'to-string to-string)
  (list 'to-symbol to-symbol)
  (list 'get-substring-by-regex get-substring-by-regex)
  (list 'replace-substring-by-regex replace-substring-by-regex)
  (list 'string-split string-split)
  (list 'string-join string-join)
  (list 'object-list->string-list object-list->string-list)
  (list 'string-remove-whitespace string-remove-whitespace)
  (list 'replace-substring-by-regex replace-substring-by-regex)
  (list 'does-string-match-glob? does-string-match-glob?)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
