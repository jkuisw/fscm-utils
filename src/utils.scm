;*******************************************************************************
; Implements some general utility procedures.

;*******************************************************************************
; Transcripts the given commands by using the fluent tui commands
; "file/start-transcript" and "file/stop-transcript". The transcript will write
; to the output file passed as parameter.
;
; Parameters:
;   commands - The commands to execute, must be a function with no parameters.
;   outputfile - The output file where to write the transcipt.
(define (transcipt-commands commands outputfile)
  ; Check if filename was set and is a valid string.
  (if (not (string? outputfile))
    (set! outputfile (get-unique-file-path "." "tmp" "trn"))
  )

  ; Check if the file already exists.
  (define startTrans "file/start-transcript")
  (if (file-exists? outputfile) (begin
      (ti-menu-load-string (format #f "~a \"~a\" ok" startTrans outputfile))
    ) (begin ; else
      (ti-menu-load-string (format #f "~a \"~a\"" startTrans outputfile))
    )
  )
  (newline)

  ; Transcript commands.
  (commands)
  (newline)

  ; Stop transcript.
  (ti-menu-load-string "file/stop-transcript")
  (newline)
)

;*******************************************************************************
; This procedure reads the contents of a file and converts it to a list of
; scheme objects. In the file the objects are seperated by whitespace.
;
; Parameters:
;   inputfile - The file to read from.
;
; Returns: The object list or false if the file does not exist.
(define (file-content-to-list inputfile) (let (
    (file-port (if (file-exists? inputfile)
      (open-input-file inputfile)
      #f ; File does not exist.
    ))
    (chars-list '())
    (tmp-char #\space)
    (finished #f)
  )

  ; Only read from file if it exists.
  (if (not (eqv? file-port #f)) (begin
    ; Loop to build the objects list. Each character of the file will be read.
    ; Objects are seperated by whitespace in the file. So if whitespace is read,
    ; than an object is added to the objects list and if there is more in the
    ; file to read, than the next object will by read character by character.
    ; This must be done to preserve the case sensitivity if the object is a
    ; string.
    (let object-loop ()
      ; Loop for reading one object character by character. The loop returns
      ; a list of character of which the object consists of.
      (set! chars-list (let read-chars-loop ((char (read-char file-port)))
        (cond
          ; Check if reached end of file, if so close it, set the finished flag
          ; and return an empty list element.
          ((eof-object? char) (begin
            (close-input-port file-port)
            (set! finished #t)
            '() ; Return the empty list element to close the character list.
          ))
          ; Check if the current character is a whitespace character.
          ((char-whitespace? char) (begin
            ; Current character is a whitespace character, therefore check the
            ; next one. If the next one is also a whitespace character than
            ; consume all whitespace character as long as there are one. If the
            ; next character is not a whitespace character, than the end of the
            ; object is found and the end of the character list is reached.
            (set! tmp-char (peek-char file-port))
            (if (or (eof-object? tmp-char) (char-whitespace? tmp-char))
              (read-chars-loop (read-char file-port))
              '() ; Return the empty list element to close the character list.
            )
          ))
          ; Current character is not end of file and is not a whitespace
          ; character. Therefore add it to the character list and procede with
          ; the next one (next loop call).
          (else (cons char (read-chars-loop (read-char file-port))))
        )
      ))
      ; If reached end of file, add the currently read object to the objects
      ; list and return. Otherwise add the object and read the next object.
      (if (eqv? finished #t)
        (if (null? chars-list)
          '() ; No characters in character list.
          (cons (list->string chars-list) '())
        )
        (cons (list->string chars-list) (object-loop))
      )
    )
  ) (begin ; else
    #f ; Return false, because file does not exist.
  ))
))

;*******************************************************************************
; Executes the given commands and returns the output of this commands as an
; object list. See [transcipt-commands](#transcipt-commands) and 
; [file-content-to-list](#file-content-to-list) for details.
;
; Parameters:
;   commands - The commands to execute, must be a function with no parameters.
;
; Returns: The output of the commands as an object list or false if the
; transcript file does not exist.
(define (cmd-output-to-object-list commands) (let (
    (transcriptfile (get-unique-file-path "." "tmp" "trn"))
    (dataList '())
  )
  ; Transcript commands to the transcript file.
  (transcipt-commands commands transcriptfile)

  ; Parse the transcript file to the data list.
  (set! dataList (file-content-to-list transcriptfile))

  ; Delete the transcript file if it already exists.
  (if (file-exists? transcriptfile) (remove-file transcriptfile))

  ; Return the data list.
  dataList
))

;*******************************************************************************
; Executes the given commands on the bash.
;
; Parameters:
;   cmd - The commands to execute. If it is a file path to an existing file,
;     than the commands in that file will be executed. If not, than the
;     parameter is expected to be an string of commands to execute.
;
; Returns: Zero on success, not zero otherwise.
(define (exec-on-bash cmd) (let (
   (ret 0)
  )

  (if (not (or (string? cmd) (symbol? cmd))) (error "Invalid parameter!" cmd))
  (if (symbol? cmd) (set! cmd (to-string cmd)))

  (if (file-exists? cmd) (begin
    ; cmd is a file path to an existing file, therefore execute the commands in
    ; the file.
    (set! ret (system (string-append "bash \"" cmd "\"")))
  ) (begin ; else
    ; cmd is not a file path to an existing file, therefore execute the string.
    (set! ret (system (string-append "bash -c \"" cmd "\"")))
  ))

  ret ; Return the exit code of the system command.
))

;*******************************************************************************
; Executes the given system command and returns the output of this system
; command as an object list. System commands are commands that will be executed
; on the current shell where fluent was started, see the fluent documentation
; of the scheme command `system`.
;
; Parameters:
;   system-command - A string with the system command, that will be executed on
;     the current shell.
;   execdir - [optional, default: #f] A path to the directory where the command
;     should be executed. If omitted, than the current directory will be used.
;
; Returns: The output of the system command as an object list or false in case
;   of an error.
(define (sys-cmd-output-to-object-list system-command . args) (let (
    (basename "tmp")
    (suffix "out")
    (tmp-file-path "")
    (object-list '())
    (backup-directory "")
    (cmd "")
    (execdir #f)
    (ret 0)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Execute directory parameter passed.
    (set! execdir (list-ref args 0))
  ))

  (if (and (not (eqv? execdir #f)) (string? execdir)) (begin
      ; Execute the system command in the executable directory (exec dir).
      ; Save current directory.
      (set! object-list (sys-cmd-output-to-object-list "pwd"))
      (if (not (eqv? object-list #f))
        (set! backup-directory (list-ref object-list 0))
        (set! ret -1)
      )

      ; Build the system command and execute it.
      (if (= ret 0) (begin
        (set! tmp-file-path (get-unique-file-path execdir basename suffix))
        (set! cmd (format #f "cd '~a' && ~a > '~a' && cd '~a'"
          execdir system-command tmp-file-path backup-directory))
        (set! ret (exec-on-bash cmd))
      ))

      (if (= ret 0) (begin
        ; Read the system command output from the tmp file to the object list.
        (set! object-list (file-content-to-list tmp-file-path))
        (remove-file tmp-file-path)
      ) (set! object-list #f))
    ) (begin ; else
      ; No valid executable directory (exec dir) given, therefore execute
      ; system command in current directory.
      ; Build the system command and execute it.
      (set! tmp-file-path (get-unique-file-path "." basename suffix))
      (set! cmd (format #f "~a > '~a'" system-command tmp-file-path))
      (set! ret (exec-on-bash cmd))

      (if (= ret 0) (begin
        ; Read the system command output from the tmp file to the object list.
        (set! object-list (file-content-to-list tmp-file-path))
        (remove-file tmp-file-path)
      ) (set! object-list #f))
    )
  )

  object-list
))

;*******************************************************************************
; Builds an index operation depending on the given `indexDefinition`. The
; `indexDefinition` has the following syntax: `(-|+)deltaIndex`  
; e.g.: `"-1",` `"+3"`, `"-14"`, ...  
; The procedure will generate a function that adds or subtracts the deltaIndex
; from the passed number. e.g.: `"+3"` will generate a function that adds three
; to the passed number and returns the result.
;
; Parameters:
;   indexDefinition - The index definition as described above.
;
; Returns: The generated operation (function).
(define (build-index-operation indexDefinition) (let (
    (operation (substring indexDefinition 0 1))
    (deltaIndex (string->number
      (substring indexDefinition 1 (string-length indexDefinition))
    ))
  )
  ;(display (format #f "indexDefinition = ~s\n" indexDefinition))
  ;(display (format #f "operation = ~s\n" operation))
  ;(display (format #f "deltaIndex = ~d\n" deltaIndex))
  (if (string=? operation "-")
    ; deltaIndex will be subtracted from index
    (lambda (idx) (- idx deltaIndex))
    ; deltaIndex will be added to index
    (lambda (idx) (+ idx deltaIndex))
  )
))

;*******************************************************************************
; Searches for a value in the object list identified by a token and name. The
; procedure searches for an object that is equal to the token. If it has found
; the token, than it checks nameIdx objects backwards if this object is equal
; to name, if so it returns the object valueIdx forward to the token, e.g.:  
;
;   + object-list = `(a b c d e f g e h i j k)`
;   + token = `"f"`
;   + nameIdx = `"-2"`
;   + name = `"d"`
;   + valueIdx = `"+3"`
;   + => returned value = `"h"`
;
; Parameters:
;   objectList - The object list to search in.
;   name - The name to match against the name object.
;   nameIdx - The index definition (see the 
;     [build-index-operation](#build-index-operation) procedure)
;     for the position of the name object relative from the position of the
;     token object.
;   token - The token to match against the token value.
;   valueIdx - The index definition (see the 
;     [build-index-operation](#build-index-operation) procedure)
;     for the position of the value object relative from the position of the
;     token object.
;   numMatch - An integer number that defines which match should be returned.
;     e.g.: The fisrt match, or second or third and so on.
;
; Returns: The value of the found object or the symbol `'not-found` if it was 
;   not found.
(define (find-in-object-list objectList name nameIdx token valueIdx numMatch)
  (let (
    (getNameIdx (build-index-operation nameIdx))
    (currNameIdx 0)
    (validNameIdx #f)
    (getValueIdx (build-index-operation valueIdx))
    (currValueIdx 0)
    (validValueIdx #f)
    (currObj "")
    (matchingName "")
    (value 'not-found)
    (matchCount 0)
    (listLength (length objectList))
  )
  ; Iterate through all objects of the list.
  (do ((i 0 (+ i 1)) (break 0)) ((or (>= i listLength) (> break 0)))
    (set! currObj (to-string (list-ref objectList i)))
    ;(display (format #f "object ~d: ~s\n" i currObj))

    ; Check if name index is in a valid range.
    (set! currNameIdx (getNameIdx i))
    (set! validNameIdx (and (> currNameIdx 0) (< currNameIdx listLength)))

    ; Check if value index is in a valid range.
    (set! currValueIdx (getValueIdx i))
    (set! validValueIdx (and (> currValueIdx 0) (< currValueIdx listLength)))

    ; Check if current list object is a token.
    (if (and validNameIdx validValueIdx (string-ci=? currObj token)) (begin
        (set! matchingName (to-string (list-ref objectList currNameIdx)))
        ;(display (format #f "-> i = ~d\n" i))
        ;(display (format #f "-> Found token, name = ~s\n" matchingName))
        (if (string-ci=? matchingName name) (begin
            ;(display (format #f "-> Found object by name: ~s\n" matchingName))
            ;(display (format #f "-> value = ~s\n" (list-ref objectList currValueIdx)))
            ;(display (format #f "-> i = ~d\n\n" i))
            ; Found object by name, get it`s value.
            (set! value (list-ref objectList currValueIdx))

            ; Check match count if finished.
            (set! matchCount (+ matchCount 1))
            (if (= matchCount numMatch)
              ; Found match, break out of the loop.
              (set! break 1)
            )
          )
        )
      )
    )
  )
  value ; return result
))

;*******************************************************************************
; Sets the value of an rp variable. If the rp variable does not exist, than it
; will be created and initialized with the value.
;
; Parameters:
;   name - The name of the rp variable.
;   value - The value for the rp variable.
;   type - The type of the rp variable, see the fluent documentation for
;     possible types.
(define (set-or-create-rpvar name value type)
  ; Only create rp variable if it not exists.
  (if (rp-var-object name)
    (rpsetvar name value)
    (rp-var-define name value type #f)
  )
)

;*******************************************************************************
; Get the fluent install directory, e.g. `"/software/ansys_inc/v162/fluent"`.
;
; Returns: Path to the fluent install directory as string.
(define (get-fluent-install-dir) (let (
    (fluent-exec-path (car (fluent-exec-name)))
  )
  ; The fluent install dir is four directories above the fluent exec directory.
  (directory
    (directory
      (directory
        (directory fluent-exec-path)
      )
    )
  )
))

;*******************************************************************************
; Retrieves the current solver version, which is one of:
;   `"2d"`, `"2ddp"`, `"3d"`, `"3ddp"`
;
; Returns: The solver version as string.
(define (get-fluent-solver-version) (let (
    (props-list (string-split (car (inquire-version)) #\,))
  )
  (string-remove-whitespace (string-append (car props-list)
    (car (cdr props-list))))
))

;*******************************************************************************
; Retrieve the current running fluent version.
;
; Returns: A key value list (map) with the following keys:  
;
;   + `'version-string` - The fluent version string (`"<major>.<minor>.<patch>"`)
;   + `'major` - The fluent major version.
;   + `'minor` - The fluent minor version.
;   + `'patch` - The fluent patch version.
(define (get-fluent-version) (let (
    (fluent-exec-path (car (fluent-exec-name)))
    (fluent-version '())
    (version-list '())
  )
  (set! version-list (string-split (strip-directory fluent-exec-path) #\.))

  (set! fluent-version (list
    (list 'version-string (string-join (cdr version-list) "."))
    (list 'major (list-ref version-list 1))
    (list 'minor (list-ref version-list 2))
    (list 'patch (list-ref version-list 3))
  ))

  ; Return the result version list.
  fluent-version
))

;*******************************************************************************
; Retrieves the current running fluent version string.
;
; Returns: The version string.
(define (get-fluent-version-string) (let (
    (fluent-exec-path (car (fluent-exec-name)))
    (version-list '())
  )
  (set! version-list (string-split (strip-directory fluent-exec-path) #\.))

  ; Return the version string.
  (string-join (cdr version-list) ".")
))

;*******************************************************************************
; Generates a file name that does not exist in the given directory, based on the
; given parameters according to the following syntax:
; `<directory>/<basename><index>[.<suffix>]`  
; The unique file path is than returned.
;
; Parameters:
;   directory - The directory for the file.
;   basename - The prefix of the file name.
;   suffix - [optional, default: ""] The file name extension.
;
; Returns: The unique file path.
(define (get-unique-file-path directory basename . args) (letrec (
    (suffix "")
    (idx 0)
    (get-file-path (lambda (dir name i s)
      (if (> (string-length s) 0)
        (string-append dir "/" name (to-string i) "." s)
        (string-append dir "/" name (to-string i))
      )
    ))
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Suffix parameter passed.
    (set! suffix (list-ref args 0))
  ))

  (let loop ((filepath (get-file-path directory basename idx suffix)))
    (set! idx (+ idx 1))
    (if (file-exists? filepath)
      (loop (get-file-path directory basename idx suffix))
      filepath ; Found unique file path, therefore return it.
    )
  )
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'transcipt-commands transcipt-commands)
  (list 'file-content-to-list file-content-to-list)
  (list 'cmd-output-to-object-list cmd-output-to-object-list)
  (list 'exec-on-bash exec-on-bash)
  (list 'sys-cmd-output-to-object-list sys-cmd-output-to-object-list)
  (list 'build-index-operation build-index-operation)
  (list 'find-in-object-list find-in-object-list)
  (list 'set-or-create-rpvar set-or-create-rpvar)
  (list 'get-fluent-install-dir get-fluent-install-dir)
  (list 'get-fluent-solver-version get-fluent-solver-version)
  (list 'get-fluent-version get-fluent-version)
  (list 'get-fluent-version-string get-fluent-version-string)
  (list 'get-unique-file-path get-unique-file-path)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
