:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: string-utils.scm
This part of the module implements utility procedures for string operations.

:arrow_right: [Go to source code of this file.](/src/string-utils.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: to-string](#to-string) | Converts the given object to a string. |
| [:page_facing_up: to-symbol](#to-symbol) | Converts an object to a symbol. |
| [:page_facing_up: get-substring-by-regex](#get-substring-by-regex) | Returns the substring of the given string by evaluating the given regularexp... |
| [:page_facing_up: replace-substring-by-regex](#replace-substring-by-regex) | Replaces a substring in a given string by a given replacement string. Thesub... |
| [:page_facing_up: string-split](#string-split) | Splits the given string by the given character. |
| [:page_facing_up: string-join](#string-join) | Joins a list of string together to one string. When a seperator string ispas... |
| [:page_facing_up: object-list->string-list](#object-list-string-list) | Converts all elements of the given list to strings. |
| [:page_facing_up: string-remove-whitespace](#string-remove-whitespace) | Removes all whitespace characters from the given string. |
| [:page_facing_up: does-string-match-glob?](#does-string-match-glob) | Checks if the given string matches the given glob-pattern. |

## Procedure Documentation

### to-string

#### Syntax
```scheme
(to-string obj)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L11)

#### Export Name
`<import-name>/to-string`

#### Description
Converts the given object to a string.

#### Parameters
##### `obj`  
The object to convert.


#### Returns
The object converted to a string.

-------------------------------------------------
### to-symbol

#### Syntax
```scheme
(to-symbol obj)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L31)

#### Export Name
`<import-name>/to-symbol`

#### Description
Converts an object to a symbol.

#### Parameters
##### `obj`  
The object to convert.


#### Returns
The object converted to a symbol.

-------------------------------------------------
### get-substring-by-regex

#### Syntax
```scheme
(get-substring-by-regex str regex)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L49)

#### Export Name
`<import-name>/get-substring-by-regex`

#### Description
Returns the substring of the given string by evaluating the given regular
expression. For the regular expression matching the system command `grep` is
used, therefore the supported regex features are the one of the "grep" system
command.

#### Parameters
##### `str`  
The string of which to extract the substring.

##### `regex`  
The regular expression to match the substring in the string.


#### Returns
The matched substring.

-------------------------------------------------
### replace-substring-by-regex

#### Syntax
```scheme
(replace-substring-by-regex str regex replace)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L75)

#### Export Name
`<import-name>/replace-substring-by-regex`

#### Description
Replaces a substring in a given string by a given replacement string. The
substring found by the given regular expression. This operation is done by the
system command `sed`. Therefore the supported regex features are the one of
the `sed` system command.

#### Parameters
##### `str`  
The string where the substring should be replaced.

##### `regex`  
The regular expression to find the substring.

##### `replace`  
The replacement string.


#### Returns
The string with the replaced substring or an empty string if the
substring could not be found.

-------------------------------------------------
### string-split

#### Syntax
```scheme
(string-split str ch)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L96)

#### Export Name
`<import-name>/string-split`

#### Description
Splits the given string by the given character.

#### Parameters
##### `str`  
The string to split.

##### `ch`  
The split character.


#### Returns
A list of the substrings splitted by the given character.

-------------------------------------------------
### string-join

#### Syntax
```scheme
(string-join str-list [seperator])
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L147)

#### Export Name
`<import-name>/string-join`

#### Description
Joins a list of string together to one string. When a seperator string is
passed, than the strings will be joined with the seperator string between
them.

#### Parameters
##### `str-list`  
A list of strings.

##### `seperator`  
_Attributes: optional, default: `""`_  
The seperator string that will be
inserted between the strings of the list.


#### Returns
The joined string or false when failed.

-------------------------------------------------
### object-list->string-list

#### Syntax
```scheme
(object-list->string-list lst)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L202)

#### Export Name
`<import-name>/object-list->string-list`

#### Description
Converts all elements of the given list to strings.

#### Parameters
##### `lst`  
The list to convert.


#### Returns
A list with all elements converted to strings.

-------------------------------------------------
### string-remove-whitespace

#### Syntax
```scheme
(string-remove-whitespace str)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L228)

#### Export Name
`<import-name>/string-remove-whitespace`

#### Description
Removes all whitespace characters from the given string.

#### Parameters
##### `str`  
The string from which to remove the whitespace characters.


#### Returns
The string without whitespace characters

-------------------------------------------------
### does-string-match-glob?

#### Syntax
```scheme
(does-string-match-glob? string glob-pattern)
```

:arrow_right: [Go to source code of this procedure.](/src/string-utils.scm#L285)

#### Export Name
`<import-name>/does-string-match-glob?`

#### Description
Checks if the given string matches the given glob-pattern.

#### Parameters
##### `string`  
The string to match against.

##### `glob-pattern`  
The globbing pattern the string should match against.


#### Returns
True if the glob-pattern matches the string, false otherwise.

