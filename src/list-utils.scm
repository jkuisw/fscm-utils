;*******************************************************************************
; Provides some utility procedures for scheme lists.

;*******************************************************************************
; Appends a value to the given list.
;
; Parameters:
;   lst - The list to which the value should be appended.
;   value - The value to append.
;
; Returns: True if successfully appended the value, false otherwise.
(define (append-to-list lst value)
  (cond
    ; Return false if list is empty
    ((null? lst) #f)
    ; If first element is '() (= null, empty element) then set car with value.
    ((and (= (length lst) 1) (null? (car lst))) (begin
      (set-car! lst value)
      #t ; successfully appended, therefore return true
    ))
    ; Otherwise append to list.
    (else (begin
      (set-cdr! (list-tail lst (- (length lst) 1)) (list value))
      #t ; successfully appended, therefore return true
    ))
  )
)

;*******************************************************************************
; Retrieves the value of a key in a map. A map is a list of pairs (lists with
; two entries) and a pair consists of a key (first entry) and a corresponding
; value (second entry).
;
; Parameters:
;   map - The map from which to retrieve the value of the key.
;   key - The key from which to read the value.
;
; Returns: The value of the key or if the key was not found than the symbol
;   `'key-not-found`.
(define (map-get-value map key)
  (let next-entry ((lst map) (curr-entry '()))
    ; Check if reached end of list and return the symbol 'key-not-found if so.
    (if (null? lst) (begin
      ; Key not found.
      'key-not-found
    ) (begin ; else
      (set! curr-entry (car lst))
      (cond
        ; Check if current entry has the requested key. If so return it`s value.
        ((eqv? key (car curr-entry)) (list-ref curr-entry 1))
        ; Current entry has not the key, therefore go to the next entry.
        (else (next-entry (cdr lst) '()))
      )
    ))
  )
)

;*******************************************************************************
; Sets the value of a key in a map. A map is a list of pairs (lists with two
; entries) and a pair consists of a key (first entry) and a corresponding value
; (second entry).
;
; Parameters:
;   map - The map where to set the value of the key.
;   key - The key in the map, where to write the value.
;   value - The value for the key.
(define (map-set-value map key value) (let (
    (current-value (map-get-value map key))
  )
  (if (eqv? current-value 'key-not-found) (begin
    ; Key does currently not exist, therefore create it.
    (append-to-list map (list key value))
  ) (begin ; else
    ; Key does exist, therefore check if the value has changed and if so update
    ; the value.
    (if (not (eqv? current-value value))
      ; Value of key has changed, therefore update it.
      (let next-entry ((lst map))
        (cond
          ; This should not occur, because checked before that the key exists.
          ((null? lst) (error (format #f
            "Failed to set map value! key = ~a, value = ~a"
            key value) map)
          )
          ; Check if found key and if so set it`s value.
          ((eqv? key (car (car lst))) (set-car! lst (list key value)))
          ; Current entry has not the key, therefore go to the next one.
          (else (next-entry (cdr lst)))
        )
      )
    )
  ))
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'append-to-list append-to-list)
  (list 'map-get-value map-get-value)
  (list 'map-set-value map-set-value)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
