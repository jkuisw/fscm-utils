# Module: fscm-utils
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: list-utils](/doc/list-utils.md) | Provides some utility procedures for scheme lists. |
| [:page_facing_up: string-utils](/doc/string-utils.md) | This part of the module implements utility procedures for string operations. |
| [:page_facing_up: utils](/doc/utils.md) | Implements some general utility procedures. |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the internal procedure names. Normally this is the same as the exported name, prepended with the import name, but thy can differ. Look into the detailed procedure documentation to get the export name of the procedure. Exported procedures are imported into a source file with the `import` procedure (see the fscm-module-manager module for further details), which takes the import name as the first parameter. Therefore every exported procedure of a module will be imported according to the following syntax:  
`<import name>/<export name of the procedure>`.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: append-to-list](/doc/list-utils.md#append-to-list) | Appends a value to the given list. |
| [:page_facing_up: map-get-value](/doc/list-utils.md#map-get-value) | Retrieves the value of a key in a map. A map is a list of pairs (lists witht... |
| [:page_facing_up: map-set-value](/doc/list-utils.md#map-set-value) | Sets the value of a key in a map. A map is a list of pairs (lists with twoen... |
| [:page_facing_up: to-string](/doc/string-utils.md#to-string) | Converts the given object to a string. |
| [:page_facing_up: to-symbol](/doc/string-utils.md#to-symbol) | Converts an object to a symbol. |
| [:page_facing_up: get-substring-by-regex](/doc/string-utils.md#get-substring-by-regex) | Returns the substring of the given string by evaluating the given regularexp... |
| [:page_facing_up: replace-substring-by-regex](/doc/string-utils.md#replace-substring-by-regex) | Replaces a substring in a given string by a given replacement string. Thesub... |
| [:page_facing_up: string-split](/doc/string-utils.md#string-split) | Splits the given string by the given character. |
| [:page_facing_up: string-join](/doc/string-utils.md#string-join) | Joins a list of string together to one string. When a seperator string ispas... |
| [:page_facing_up: object-list->string-list](/doc/string-utils.md#object-list-string-list) | Converts all elements of the given list to strings. |
| [:page_facing_up: string-remove-whitespace](/doc/string-utils.md#string-remove-whitespace) | Removes all whitespace characters from the given string. |
| [:page_facing_up: does-string-match-glob?](/doc/string-utils.md#does-string-match-glob) | Checks if the given string matches the given glob-pattern. |
| [:page_facing_up: transcipt-commands](/doc/utils.md#transcipt-commands) | Transcripts the given commands by using the fluent tui commands"file/start-t... |
| [:page_facing_up: file-content-to-list](/doc/utils.md#file-content-to-list) | This procedure reads the contents of a file and converts it to a list ofsche... |
| [:page_facing_up: get-fluent-version](/doc/utils.md#get-fluent-version) | Retrieve the current running fluent version. |
| [:page_facing_up: get-fluent-version-string](/doc/utils.md#get-fluent-version-string) | Retrieves the current running fluent version string. |
| [:page_facing_up: get-unique-file-path](/doc/utils.md#get-unique-file-path) | Generates a file name that does not exist in the given directory, based on t... |
| [:page_facing_up: cmd-output-to-object-list](/doc/utils.md#cmd-output-to-object-list) | Executes the given commands and returns the output of this commands as anobj... |
| [:page_facing_up: exec-on-bash](/doc/utils.md#exec-on-bash) | Executes the given commands on the bash. |
| [:page_facing_up: sys-cmd-output-to-object-list](/doc/utils.md#sys-cmd-output-to-object-list) | Executes the given system command and returns the output of this systemcomma... |
| [:page_facing_up: build-index-operation](/doc/utils.md#build-index-operation) | Builds an index operation depending on the given `indexDefinition`. The`inde... |
| [:page_facing_up: find-in-object-list](/doc/utils.md#find-in-object-list) | Searches for a value in the object list identified by a token and name. Thep... |
| [:page_facing_up: set-or-create-rpvar](/doc/utils.md#set-or-create-rpvar) | Sets the value of an rp variable. If the rp variable does not exist, than it... |
| [:page_facing_up: get-fluent-install-dir](/doc/utils.md#get-fluent-install-dir) | Get the fluent install directory, e.g. `"/software/ansys_inc/v162/fluent"`. |
| [:page_facing_up: get-fluent-solver-version](/doc/utils.md#get-fluent-solver-version) | Retrieves the current solver version, which is one of:  `"2d"`, `"2ddp"`, `"... |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

  
The module does not have any private procedures.  
  

